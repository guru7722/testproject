import React from 'react';
import Navbar from './Navbar';
export default function MainFrame() {
  return (
    <>
    <div className='main-frame'>
      <Navbar />
      <h1>Fun facts about React</h1>
    <ul className='od-list'>
      <li>Was first released in 2013</li>
      <li>Was originally created by Jordan Walke</li>
      <li>Has well over 100K stars on GitHub</li>
      <li>Is maintained by Facebook</li>
      <li>Powers thousands of enterprise apps, including mobile apps</li>
    </ul>
    <footer>
      <small>@ 2022 ViP devlopment .All rights are reserved</small>
    </footer>
    </div>
  
    </>
  );
}
